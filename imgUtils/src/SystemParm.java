


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class SystemParm {


	public static final Properties SystemParm = new Properties();
	
	public static void load(String path) {
		try { 
			
			SystemParm.load(new InputStreamReader(new FileInputStream(path), "UTF-8"));
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getParm(String parmName){
		String value = SystemParm.getProperty(parmName);
		if (value == null)
			value = "";
		return value.trim();
	}
	
	public static String getSrc() {
		return getParm("src");
	}
	
	public static String getDes() {
		return getParm("des");
	}
	
	public static int getSize() {
		return Integer.parseInt(getParm("size"));
	}
	
	public static String getSrcTwo() {
		return getParm("two");
	}


	public static int getQXCategory(){
		String value = SystemParm.getProperty("QX_PURCHASE_CATEGORY_ID");
		if (CommonUtils.isEmpty(value))
			return 0;
		return Integer.parseInt(value);
	}

}
