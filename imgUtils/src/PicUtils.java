

import java.io.*;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.coobird.thumbnailator.Thumbnails;

public class PicUtils {


    /**
     * 解压图片文件夹
     */
    private final int fileSize = 200;


    //以下是常量,按照阿里代码开发规范,不允许代码中出现魔法值
    private static final Integer ZERO = 0;
    private static final Integer two_zero_zero = 200;
    private static final Integer ONE_ZERO_TWO_FOUR = 1024;
    private static final Integer NINE_ZERO_ZERO = 900;
    private static final Integer THREE_TWO_SEVEN_FIVE = 3275;
    private static final Integer FIVE_ONE_TWO_ZERO = 5120;
    private static final Integer TWO_ZERO_FOUR_SEVEN = 2047;
    private static final Double ZERO_EIGHT_FIVE = 0.95;
    private static final Double ZERO_SIX = 0.8;
    private static final Double ZERO_FOUR_FOUR = 0.64;
    private static final Double ZERO_FOUR = 0.6;
    private static final int factor = 2;
    private Set<String> picExtSet = new HashSet<String>();

    public PicUtils() {
        picExtSet.add(".jpg");
        picExtSet.add(".gif");
        picExtSet.add(".png");
        picExtSet.add(".jpeg");

    }

    /**
     * 根据指定大小压缩图片
     *
     * @param imageBytes  源图片字节数组
     * @param desFileSize 指定图片大小，单位kb
     * @return 压缩质量后的图片字节数组
     */
    public static byte[] compressPicForScale(byte[] imageBytes, long desFileSize) {
        if (imageBytes == null || imageBytes.length <= ZERO || imageBytes.length < desFileSize * ONE_ZERO_TWO_FOUR) {
            return imageBytes;
        }
        long srcSize = imageBytes.length;

        double accuracy = getAccuracy(srcSize / ONE_ZERO_TWO_FOUR);
        try {
            while (imageBytes.length > (desFileSize * ONE_ZERO_TWO_FOUR * factor)) {
                ByteArrayInputStream inputStream = new ByteArrayInputStream(imageBytes);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream(imageBytes.length);
                Thumbnails.of(inputStream)
                        .scale(accuracy)
                        .outputQuality(1)
                        .toOutputStream(outputStream);
                imageBytes = outputStream.toByteArray();
            }
            System.out.println("图片原大小={" + srcSize / ONE_ZERO_TWO_FOUR + "}kb | 压缩后大小={" + imageBytes.length / ONE_ZERO_TWO_FOUR + "}kb");
        } catch (Exception e) {
            System.out.println("图片压缩msg=图片压缩失败!");
            e.printStackTrace();
        }
        return imageBytes;
    }


    /**
     * 自动调节精度(经验数值)
     *
     * @param size 源图片大小
     * @return 图片压缩质量比
     */
    private static double getAccuracy(long size) {
        double accuracy;
        if (size < NINE_ZERO_ZERO) {
            accuracy = ZERO_EIGHT_FIVE;
        } else if (size < TWO_ZERO_FOUR_SEVEN) {
            accuracy = ZERO_SIX;
        } else if (size < THREE_TWO_SEVEN_FIVE) {
            accuracy = ZERO_FOUR_FOUR;
        } else {
            accuracy = ZERO_FOUR;
        }
        return accuracy;
    }


    /**
     * 第二压缩方法，用于图片覆盖
     * @throws IOException
     */
    public void processTwo(String conf) throws IOException {
    	SystemParm.load(conf);
        String imgSrcFilePath = SystemParm.getSrcTwo();   //第二原图片文件夹
        String imgDesFilePath = SystemParm.getDes();  // 解压图片文件夹

        int size = fileSize;   // 指定大小
        System.out.println("==========================");
        System.out.println("==========================");
        System.out.println("=========第二次覆盖解压========");
        System.out.println("源文件目录 : " + imgSrcFilePath);
        System.out.println("目标文件目录 : " + imgDesFilePath);
        System.out.println("压缩大小 : " + size);
        if (CommonUtils.isEmpty(imgSrcFilePath) || CommonUtils.isEmpty(imgDesFilePath) || size == 0) {
            System.out.println("错误:配置文件错误");
            return;
        }

        if (!imgSrcFilePath.endsWith("\\")) {
            imgSrcFilePath += "\\";
        }

        if (!imgDesFilePath.endsWith("\\")) {
            imgDesFilePath += "\\";
        }

        // 图片文件夹
        File imgFile = new File(imgSrcFilePath);
        File zipFile = new File(imgDesFilePath);

        if (imgFile.exists() == false || zipFile.exists() == false || size < 20 || size > 400) {
            System.out.println("请检查配置文件，文件夹应该不存在。目标图片大小必须介于20和400");
            return;
        }


        File[] fileList = imgFile.listFiles(); // 原图片文件夹所有文件
        String zipFileList[] = zipFile.list(); // 解压文件夹所有文件
        List<String> zipList = Arrays.asList(zipFileList);
        // 拿到所有文件

        System.out.println("源文件数量 : " + fileList.length);
        System.out.println("目标文件数量 : " + zipList.size());
        if (fileList.length > 0) {
            int count = 0;
            for (File fileSrc : fileList) {
                if (fileSrc.isDirectory())
                    continue;

                String fileName = fileSrc.getName();

                String fileExt = fileName.substring(fileName.lastIndexOf("."));
                if (!picExtSet.contains(fileExt.toLowerCase())) {
                    System.out.println("过滤文件 : " + fileName);
                    continue;
                }
                // 直接压缩
                count++;
                compressFile(imgSrcFilePath, imgDesFilePath, fileName, size);

            }
            System.out.println("-- 压缩总数 : " + count);
        } else {
            System.out.println("输入文件夹是空");
        }

    }

    /**
     * 第一压缩方法
     * @throws IOException
     */
    public void process(String conf) throws IOException {
        SystemParm.load(conf);
        String imgSrcFilePath = SystemParm.getSrc();   //第二原图片文件夹
        String imgDesFilePath = SystemParm.getDes();  // 解压图片文件夹

        int size = fileSize;   // 指定大小
        System.out.println("源文件目录 : " + imgSrcFilePath);
        System.out.println("目标文件目录 : " + imgDesFilePath);
        System.out.println("压缩大小 : " + size);
        if (CommonUtils.isEmpty(imgSrcFilePath) || CommonUtils.isEmpty(imgDesFilePath) || size == 0) {
            System.out.println("错误:配置文件错误");
            return;
        }

        if (!imgSrcFilePath.endsWith("\\")) {
            imgSrcFilePath += "\\";
        }

        if (!imgDesFilePath.endsWith("\\")) {
            imgDesFilePath += "\\";
        }

        // 图片文件夹
        File imgFile = new File(imgSrcFilePath);
        File zipFile = new File(imgDesFilePath);

        if (imgFile.exists() == false || zipFile.exists() == false || size < 20 || size > 400) {
            System.out.println("请检查配置文件，文件夹应该不存在。目标图片大小必须介于20和400");
            return;
        }

        File[] fileList = imgFile.listFiles(); // 原图片文件夹所有文件
        String zipFileList[] = zipFile.list(); // 解压文件夹所有文件
        List<String> zipList = Arrays.asList(zipFileList);
        // 拿到所有文件

        System.out.println("源文件数量 : " + fileList.length);
        System.out.println("目标文件数量 : " + zipList.size());
        if (fileList.length > 0) {
            int count = 0;
            for (File fileSrc : fileList) {
                //System.out.println("文件  : " + fileSrc.getName());
                if (fileSrc.isDirectory())
                    continue;

                String fileName = fileSrc.getName();

                String fileExt = fileName.substring(fileName.lastIndexOf("."));
                if (!picExtSet.contains(fileExt.toLowerCase())) {
                    System.out.println("过滤文件 : " + fileName);
                    continue;
                }
                //System.out.println("文件是否存在  : " + fileSrc.getName() + ", " + zipList.contains(fileName));

                if (!zipList.contains(fileName)) {  // 不存在就压缩
                    count++;
                    compressFile(imgSrcFilePath, imgDesFilePath, fileName, size);
                } else {
                    File desFile = new File(imgDesFilePath + fileName);
                    Date desFileTime = CommonUtils.getTimeByLong(desFile.lastModified());
                    Date srcFileTime = CommonUtils.getTimeByLong(fileSrc.lastModified());

                    if (srcFileTime.after(desFileTime)) {
                        count++;
                        compressFile(imgSrcFilePath, imgDesFilePath, fileName, size);
                    }
                }
            }
            System.out.println("-- 压缩总数 : " + count);
        } else {
            System.out.println("输入文件夹是空");
        }
    }

    private void compressFile(String imgSrcFilePath, String imgDesFilePath, String fileName, int size) throws IOException {
        File file = new File(imgSrcFilePath + fileName);
        FileInputStream inputStream = new FileInputStream(file);

        byte[] bytes = new byte[10500240];
        while (true) {
            int len = inputStream.read(bytes);
            if (len == -1) {
                break;
            }
        }
        inputStream.close();
        System.out.println("压缩图片 ： " + fileName);
        byte[] imgBytes = PicUtils.compressPicForScale(bytes, size);
        File fOut = new File(imgDesFilePath + fileName);
        FileOutputStream fileOutputStream = new FileOutputStream(fOut);
        fileOutputStream.write(imgBytes);
        fileOutputStream.close();
    }
    
    
    
    public static void main(String[] args) throws IOException {

        PicUtils picUtils = new PicUtils();
        picUtils.process(args[0]);
        picUtils.processTwo(args[0]);
    }

}
